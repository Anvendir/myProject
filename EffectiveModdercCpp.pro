TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    chapter1.cpp

HEADERS += \
    chapter1.hpp
