#ifndef CHAPTER1_HPP
#define CHAPTER1_HPP


class Chapter1
{
public:
    Chapter1();

    void item1();

    template<typename T>
    void fr(T& p_param);

    template<typename T>
    void fcr(const T& param);

    template<typename T>
    void fp(T* param);

    template<typename T>
    void fcp(const T* param);
};

#endif // CHAPTER1_HPP
