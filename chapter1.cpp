#include "chapter1.hpp"
#include <iostream>
#include <boost/type_index.hpp>

using boost::typeindex::type_id_with_cvr;

Chapter1::Chapter1()
{

}


template<typename TD>
class CompileCheck;

template<typename T>
void Chapter1::fr(T& param)
{
    std::cout << "Type of param: "
              << type_id_with_cvr<decltype(param)>().pretty_name()
              << std::endl;
    std::cout << "Type of T: "
              << type_id_with_cvr<T>().pretty_name()
              << std::endl;
    std::cout << std::endl;
    //CompileCheck<T> bla;
    //CompileCheck<decltype(param)> bla;
}

template<typename T>
void Chapter1::fcr(const T& param)
{
    std::cout << "Type of param: "
              << type_id_with_cvr<decltype(param)>().pretty_name()
              << std::endl;
    std::cout << "Type of T: "
              << type_id_with_cvr<T>().pretty_name()
              << std::endl;
    std::cout << std::endl;
}

template<typename T>
void Chapter1::fp(T* param)
{
    std::cout << "Type of param: "
              << type_id_with_cvr<decltype(param)>().pretty_name()
              << std::endl;
    std::cout << "Type of T: "
              << type_id_with_cvr<T>().pretty_name()
              << std::endl;
    std::cout << std::endl;
    //CompileCheck<T> bla;
    //CompileCheck<decltype(param)> bla;
}

template<typename T>
void Chapter1::fcp(const T* param)
{
    std::cout << "Type of param: "
              << type_id_with_cvr<decltype(param)>().pretty_name()
              << std::endl;
    std::cout << "Type of T: "
              << type_id_with_cvr<T>().pretty_name()
              << std::endl;
    std::cout << std::endl;
}

void Chapter1::item1()
{
    std::cout << "Case one: "
              << "Parameter is Reference or Pointer but no Universal Reference!\n"
              << std::endl;

    int x = 27;
    const int cx = x;
    const int& rx = x;
    int* px = &x;
    const int* cpx = &x;

    //CompileCheck<decltype(x)> bla;
    //CompileCheck<decltype(cx)> bla;
    //CompileCheck<decltype(rx)> bla;

    std::cout << "Type of arg x: "
              << type_id_with_cvr<decltype(x)>().pretty_name()
              << std::endl;
    fr(x);
    fcr(x);
    std::cout << std::endl;

    std::cout << "Type of arg cx: "
              << type_id_with_cvr<decltype(cx)>().pretty_name()
              << std::endl;
    fr(cx);
    fcr(cx);
    std::cout << std::endl;

    std::cout << "Type of arg rx: "
              << type_id_with_cvr<decltype(rx)>().pretty_name()
              << std::endl;
    fr(rx);
    fcr(rx);
    std::cout << std::endl;

    std::cout << "Type of arg px: "
              << type_id_with_cvr<decltype(px)>().pretty_name()
              << std::endl;
    fr(px);
    fcr(px);
    fp(px);
    fcp(px);
    std::cout << std::endl;

    std::cout << "Type of arg cpx: "
              << type_id_with_cvr<decltype(cpx)>().pretty_name()
              << std::endl;
    fr(cpx);
    fcr(cpx);
    fp(cpx);
    fcp(cpx);
    std::cout << std::endl;
}

